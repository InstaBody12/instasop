﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INSTAGRAMSHOP.Models
{
    public class Sales
    {
        private int Id { get; set; }

        private DateTime Date { get; set; }

        private int TotalPrice { get; }

        private List<Goods> List { get; set; }

        private Client Client { get; set; }

        public Sales( int totalPrice )
        {
            Id = 1;
            Date = DateTime.Today;
            TotalPrice = totalPrice;

        }
    }
}
