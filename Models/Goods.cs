﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INSTAGRAMSHOP.Models
{
    public class Goods
    {
        public Goods(string name, string image, int amount, int priceToSales, int priceToBuy)
        {
            Name = name;
            this.image = image;
            Amount = amount;
            PriceToSales = priceToSales;
            PriceToBuy = priceToBuy;
        }
        public void IncrementCount(int GoodsAmount)
        {
            Amount += GoodsAmount;
        }


        private string Id { get; }
        private string Name { get; set; }

        private string image { get; set; }

        private int Amount { get; set; }

        private int PriceToSales { get; set; }

        private int PriceToBuy { get; set; }
    }
}

