﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INSTAGRAMSHOP.Models
{
    public class Address
    {

        public Address(string city, string numberOfDelivery, string coment)
        {
            if (string.IsNullOrWhiteSpace(city))
            {
                throw new NullReferenceException("City cannot be NULL.");
            }

            if (string.IsNullOrWhiteSpace(numberOfDelivery))
            {
                throw new NullReferenceException("NumberOfDelivery cannot be NULL.");
            }


            City = city;
            NumberOfDelivery = numberOfDelivery;
            Coment = coment;
        }

        private string Id { get; }
        private string City { get; set; }

        private string NumberOfDelivery { get; set; }

        private string Coment { get; set; }
    }
}
