﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INSTAGRAMSHOP.Models
{
    public class Client
    {
        private string Id { get; }
        private string LastName { get; set; }

        private string FirstName { get; set; }

        private Address address { get; set; }

        private int SalesAmount { get; set; }

        private string PhoneNumber { get; set; }

        private int CountOfSales { get; set; }

        public Client(string lastName, string firstName, Address addressParam, string phoneNumber)
        {


            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new NullReferenceException("LastName cannot be NULL.");
            }

            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new NullReferenceException("FirstName cannot be NULL.");
            }

            if (addressParam == null)
            {
                throw new NullReferenceException("Addres cannot be NULL.");
            }


            if (string.IsNullOrWhiteSpace(phoneNumber))
            {
                throw new NullReferenceException("PhoneNumber cannot be NULL.");
            }

            address = addressParam;

            LastName = lastName;

            FirstName = firstName;

            PhoneNumber = phoneNumber;

            SalesAmount = 0;

            CountOfSales = 0;
        }
    }
}
