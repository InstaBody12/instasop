﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using INSTAGRAMSHOP.Models;
using System.Text.Json;
using System.IO;

namespace INSTAGRAMSHOP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Buy() 
        {
            return View();
        }
        [HttpPost]
        public IActionResult Buy(string vid,string number,string sum,string LastName,string Firstname)
        {
           
           return Content($"Ваші данні:{vid},{number},{sum},{LastName},{Firstname}"+SaveParcel(int.Parse(sum)));
        }
        public IActionResult Privacy()
        {
            return View();
        }

       
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private string SaveParcel(int sum) {
            Sales sales = new Sales(sum);
            string json = sales.ToString();
            Console.WriteLine(json);
            string writePath = "Sales.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(json);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        
            return "Посилка збережена";
        }
    }
}
